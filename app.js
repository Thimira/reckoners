var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var debug = require('debug')('reckoners');
var spawn = require('child_process').spawn;
var hbs = require('hbs');
var Datastore = require('nedb');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'handlebars');
app.engine('handlebars', require('hbs').__express);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

var db = new Datastore({ filename: path.join('DB', 'rec.db'), autoload: true });
app.db = db;

app.set('port', process.env.PORT || 3000);

var rScript;

var server = app.listen(app.get('port'), function() {
    debug('Express server listening on port ' + server.address().port);
    console.log('Express server listening on port ' + server.address().port);

    app.db.find({}, function (err, docs) {
        console.log(docs);
    });

    var rServerScript = path.join('rScripts', 'server.R');

    rScript = spawn('Rscript', [rServerScript]);

    rScript.stdout.on('data', function (data) {
        console.log('stdout: ' + data);
    });

    rScript.stderr.on('data', function (data) {
        console.log('stderr: ' + data);
    });

    rScript.on('close', function (code) {
        console.log('child process exited with code ' + code);
    });
});

process.on('uncaughtException', function(e) {
    console.log('uncaughtException');
    console.log(e);
    rScript.kill();
});

process.on('exit', function() {
    console.log('exiting');
    rScript.kill();
});
