var express = require('express');
var router = express.Router();
var agent = require('superagent');
var resemble = require('node-resemble-js');
var fs = require('fs');
var path = require('path');

//var app = express();

/* GET home page. */
router.get('/', function(req, res, next) {
    var app = req.app;

    res.render('index', { title: 'Express' });
});

router.post('/calculateKD', function(req, res, next) {
    var app = req.app;
    var kdData = req.body;
    var studentID = req.body.studentID;
    var assignmentID = req.body.assignmentID;

    var fileName = studentID + ".png";
    kdData.fileName = fileName;

    var compare = false;

    if (fs.existsSync(path.join('kd', fileName))) {
        var timeStamp = +new Date;
        kdData.fileName = studentID + "-" + timeStamp + ".png";
        compare = true;
    }

    agent.post("http://localhost:9454")
    .send(kdData)
    .timeout(1500)
    .end(function(err, res2) {
        console.log(res2.text);
        console.log(res2.body);
        if (compare) {
            var fileData = fs.readFileSync(path.join('kd', res2.body.result.toString()));
            var originalFileData = fs.readFileSync(path.join('kd', fileName));
            var diff = resemble(fileData).compareTo(originalFileData).onComplete(function(data) {
                console.log(data);

                fs.unlink(path.join('kd', res2.body.result.toString()));

                var doc = {
                    'type' : 'keyDyn',
                    'timeStamp' : +new Date,
                    'studentID': studentID,
                    'assignmentID' : assignmentID,
                    'mismatch' : data.misMatchPercentage
                };

                app.db.insert(doc, function (err, newDoc) {
                });

                res.send(data);
            });
        }
    });
});

router.get('/analyzeKD', function(req, res, next) {
    var app = req.app;
    app.db.find({ type : 'keyDyn' }, function (err, docs) {
        console.log(docs);
        agent.post("http://localhost:9454")
        .send({ method : "keystrokeAnalyze", kdData: docs })
        .timeout(1500)
        .end(function(err, res2) {
            console.log(res2.text);
            console.log(res2.body);
        });
        res.render('analyzeKD', { kdData: docs });
    });
});

router.get('/reports', function(req, res, next) {
    var app = req.app;
    app.db.find({ type : 'keyDyn' }, function (err, docs) {
        console.log(docs);
        agent.post("http://localhost:9454")
        .send({ method : "report", kdData: docs })
        .timeout(1500)
        .end(function(err, res2) {
            //console.log(res2.text);
            console.log(res2.body);
            res.render('reports', { kdData: docs, kd : res2.body.data.studentDyn, clusters : res2.body.data.clusters });
        });
    });
});

module.exports = router;
