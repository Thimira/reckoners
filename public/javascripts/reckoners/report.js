$(function() {
	$('.tableCell').each(function() {
		var bgColor = getColor(parseInt($(this).text()));
		$(this).attr('style', bgColor);
	});

	$('.tableCell2').each(function() {
		var bgColor = getColor(parseInt($(this).text()) * 10);
		$(this).attr('style', bgColor);
	});

});

function getColor(percentage) {
	percentageValue = percentage;
	// var R = Math.floor((255 * percentageValue) / 100);
	// var G = Math.floor((255 * (100 - percentageValue)) / 100);
	// var B = 0;
	if (percentageValue === 100) {
		percentageValue = 99
	}
	var r, g, b;

	if (percentageValue < 50) {	// green to yellow
		r = Math.floor(255 * (percentageValue / 50));
		g = 255;

	} else {	// yellow to red
		r = 255;
		g = Math.floor(255 * ((50 - percentageValue % 50) / 50));
	}
	b = 0;

	var rgbColor = 'background-color:rgba(' + r + ',' + g + ',' + b + ', 0.5)';
	return rgbColor;
};


function showGraph() {
	console.log("Hi");

}

function getJSONData() {
	var jsonString = {
		"reports" : {
			"studentEvaluation" : {
				"record" : [ {
					"name" : "John",
					"cheatingPossibility" : "30%",
					"groupworkPossibility" : "20%"
				}, {
					"name" : "Ram",
					"cheatingPossibility" : "23%",
					"groupworkPossibility" : "21%"
				}, {
					"name" : "Adam",
					"cheatingPossibility" : "33%",
					"groupworkPossibility" : "12%"
				}, {
					"name" : "Saman",
					"cheatingPossibility" : "23%",
					"groupworkPossibility" : "32%"
				}, {
					"name" : "Shan",
					"cheatingPossibility" : "22%",
					"groupworkPossibility" : "11%"
				},

				]
			},
			"assignmentEvaluation" : {
				"record" : [ {
					"assignment" : "John",
					"classAverage" : "65",
					"cheatingPossibility" : "23%",
					"groupworkPossibility" : "32%"
				}, {
					"assignment" : "John",
					"classAverage" : "65",
					"cheatingPossibility" : "23%",
					"groupworkPossibility" : "32%"
				}, {
					"assignment" : "John",
					"classAverage" : "45",
					"cheatingPossibility" : "23%",
					"groupworkPossibility" : "32%"
				}, {
					"assignment" : "John",
					"classAverage" : "44",
					"cheatingPossibility" : "23%",
					"groupworkPossibility" : "32%"
				}, {
					"assignment" : "John",
					"classAverage" : "34",
					"cheatingPossibility" : "23%",
					"groupworkPossibility" : "32%"
				}, ]
			}

		},
		"test" : "TEST"
	};

	return jsonString;
}
