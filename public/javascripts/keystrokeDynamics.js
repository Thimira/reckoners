$(document).ready(function() {
    var tmr = [];
    var wordgap = [];

    $('#txt').keyup(function(evt) {
        if(evt.keyCode == 13) { // ignore enter
            console.log("ENTER ignored");
            return;
        }

        if($('#txt').val().length === 0) { // reset after clear
            tmr = [];
            console.log("DELETED");
            return;
        }

        if(evt.keyCode >= 65 && evt.keyCode <= 90) {
            tmr.push({"tmr":evt.timeStamp, "cod":evt.which});
        }

        if(evt.keyCode == 32) { // space key
            wordgap.push(evt.timeStamp);
            tmr.push({"tmr":evt.timeStamp, "cod":evt.which});
        }

        console.log("Key code: "+ String.fromCharCode(evt.which) +" On time: "+evt.timeStamp + " " + evt.keyCode);
    });

    $('#sub').click(function showDynamic(){
        var prev = "";
        var keystrokes = [];
        var prevtime = tmr[0].tmr;
        var duration = tmr[tmr.length - 1].tmr - tmr[0].tmr;
        var wordgapMat = [];

        for(var i = 0; i < tmr.length - 1; ++i) {
            if (tmr[i].cod == 32) {
                wordgapMat[wordgapMat.length] = tmr[i].tmr - prevtime;
            } else {
                keystrokes[keystrokes.length] = [String.fromCharCode(prev), String.fromCharCode(tmr[i].cod), tmr[i].tmr - prevtime];
                prev = tmr[i].cod;
                prevtime = tmr[i].tmr;
            }
        }

        keystrokes.shift();

        var studentID = $('#studentID').val();
        var assignmentID = $('#assignmentID').val();

        var dataRes = {
            "method": "keystrokeGenerate",
            "char" : keystrokes,
            "gap" : wordgapMat,
            "duration" : duration,
            "studentID" : studentID,
            "assignmentID" : assignmentID
        };

        console.log(dataRes);

        $.ajax({
            type : "POST",
            url : "/calculateKD",
            data : JSON.stringify(dataRes),
            contentType : "application/json; charset=utf-8",
            success : function(data) {
                console.log(data);
                $('#result').html(data.misMatchPercentage);
            },
            failure : function(data) {
                console.log(ret2);
            }
        });
    });
});
